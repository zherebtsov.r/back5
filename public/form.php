<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
    <link rel="stylesheet" href="../style/main_style.css" />
    <!-- <link rel="stylesheet" href="style.css"> -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"
        defer></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"
        defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"
        defer></script>
    <title>Web 1</title>
</head>

<body>
    <style>
    /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
    .error {
        border: 2px solid red;
    }
    </style>
    </head>

    <body>

        <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $key => $message) {
    print($message);
}
    
  print('</div>');
}
?>

        <header>
            
            <nav class="navbar navbar-expand-lg navbar-light bg-dark mt-5">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                    aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse bg-dark" id="navbarTogglerDemo01">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a href="#header" class="nav-link text-white font-weight-light">Заголовок</a>
                        </li>
                        <li class="nav-item">
                            <a href="#form" class="nav-link text-white font-weight-light">Форма</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section id="form" class=" p-4 order-1">
            <h2 class="text-center">Форма</h2>
            <div class="form mx-auto p-3">
                <form action="./index.php" method="POST">
                    <div class="forms p-2">
                        <div>
                            <p>Имя <input name="fio" type="text" <?php if ($errors['fio']) {print 'class="error"';} ?>
                                    value="<?php print $values['fio']; ?>" /></p>
                            <p>E-mail <input name="email" type="email"
                                    <?php if ($errors['email']) {print 'class="error"';} ?>
                                    value="<?php print $values['email']; ?>" /></p>
                            <p>
                                Дата рождения
                                <input name="data" type="date"
                                    value="<?php print $values['data']; ?>" />
                            </p>
                            <p>
                                Пол:
                                <input name="gender" type="radio" id="man" value="man"
                                    <?php if ($values['gender'] == 'man') print("checked");?> />
                                <label for="man">М</label>
                                <input name="gender" type="radio" id="woman" value="male"
                                    <?php if ($values['gender'] == 'male') print("checked"); ?> />
                                <label for="woman">Ж</label>
                            </p>
                            <p>
                                Кол-во конечностей:
                                <input name="limbs" type="radio" id="one" value="1"
                                    <?php if ($values['limbs'] == '1') print("checked");?> />
                                <label for="one">1</label>
                                <input name="limbs" type="radio" id="two" value="2"
                                    <?php if ($values['limbs'] == '2') print("checked");?> />
                                <label for="one">2</label>
                                <input name="limbs" type="radio" id="three" value="3"
                                    <?php if ($values['limbs'] == '3') print("checked");?> />
                                <label for="three">3</label>
                                <input name="limbs" type="radio" id="four" value="4"
                                    <?php if ($values['limbs'] == '4') print("checked");?> />
                                <label for="four">4</label>
                            </p>
                        </div>
                        <div>
                            <p>
                                Ваши таланты:
                            <p></p>
                            <select name="talents[]" id="talents" multiple="multiple"
                                <?php if ($errors['talents']) {print 'class="error"';} ?> name="talents[]" multiple
                                size="4">
                                <?php
                                    foreach ($talents as $key => $value) {
                                        $selected = empty($values['talents'][$key]) ? '' : ' selected="selected" ';
                                        printf('<option value="%s",%s>%s</option>', $key, $selected, $value);
                                    }
                                ?>
                                <!-- <option value="1">
                        Скорочтение
                      </option>
                      <option
                        value="2"
                      >
                        Хорошая физическая подготовка
                      </option>
                      <option value="3">
                        Фотографическая память
                      </option> -->
                            </select>
                            <br />
                            <label for="bio">Ваша биография</label>
                            <br />
                            <textarea name="biography" id="bio" cols="30" rows="4" 
                                <?php if ($errors['biography']) {print 'class="error"';}?> value = <?php $values['biography']; ?>
                    ><?php print $values['biography']; ?></textarea>
                            </p>
                        </div>
                    </div>
                    <p>
                        <label <?php if ($errors['check']) {print 'class="error"';} ?> for="check">C контрактом
                            ознакомлен</label>
                        <input name="check" type="checkbox" id="check" />
                    </p>
                    <p><input type="submit" type="button" class="btn btn-success" /></p>
                </form>
            </div>
            </div>
        </section>
        <footer id="footer" class="mt-3 order-5 px-4">
            ФИО верстальщика:
            <address>Жеребцов Роман Александрович</address>
            
        </footer>
        </div>
        <hr />
    </body>

</html>