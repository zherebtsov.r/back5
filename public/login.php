<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  session_destroy();
  header('Location: ./');
}
$message;
if (!empty($_COOKIE['notsave'])) {
  setcookie('notsave', '', 100000);
  $messages[] = 'Ошибка отправления в базу данных.';
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Задание 5 - логин</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
    <link rel="stylesheet" href="../style/main_style.css">

    <script src="script.js"></script>
</head>

<header>
    <div class=" logoH1 d-flex  flex-column justify-content-center justify-content-sm-center align-items-center">
        <img class="logo mt-3"
            src="https://image.freepik.com/free-vector/creative-powerful-phoenix-logo_23-2148500609.jpg" alt="logo" />
    </div>
</header>

<body>
<div class="login mx-auto p-3">
    <form action="" method="POST">
        <label>
            Логин
            <input name="login" />
        </label>
        <label>
            Пароль
            <input name="pass" />
        </label>
        <input type="submit" value="Войти" />
    </form>
</div>

<body>

</body>
<?php
  if (!empty($_GET['none'])) {
    $txt = "Таких данных в бд НЕТ";
    print "<div>" . $txt . "<div>";
  }
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u35651';
  $pass = '2345345';
  try {
    $db = new PDO('mysql:host=localhost;dbname=u35651', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $loginId = $_POST['login'];
    $passId = md5($_POST['pass']);
    $stmt = $db->prepare("SELECT userId FROM loginData WHERE loginId = ? AND passId = ?");
    $stmt->execute([$loginId, $passId]);
    $user_id = $stmt->fetch(PDO::FETCH_COLUMN);
  } catch (PDOException $e) {
    printf($e);
    setcookie('notsave', '1');
    session_reset();
  }
  if ($user_id) {
    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uId'] = $user_id;
    $_COOKIE[session_name()] = "session_true";
    header('Location: ./');
    // Делаем перенаправление.
    //  header('Location: ./');
  } else {
    header('Location: ?none=1');
  }
}
?>