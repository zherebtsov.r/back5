<?php

header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $messages = array();
    $messages[8] = '';
    $messages[9] = '';
    if (!empty($_COOKIE['notsave'])) {
      setcookie('notsave', '', 100000);
      $messages[] = 'Ошибка отправления в базу данных.';
    }
    if (!empty($_COOKIE['save'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('save', '', 100000);
      setcookie('login', '', 100000);
      setcookie('pass', '', 100000);
      // Если есть параметр save, то выводим сообщение пользователю.
      $messages[8] = '<div style="text-align: center; margin: 4px;">Спасибо, результаты сохранены.</div>';
      // Если в куках есть пароль, то выводим сообщение.
      if (!empty($_COOKIE['pass'])) {
        $messages[9] = sprintf(
          'Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.',
          strip_tags($_COOKIE['login']),
          strip_tags($_COOKIE['pass'])
        );
      }
    }
  
  
    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = empty($_COOKIE['fio_error']) ? '' : $_COOKIE['fio_error'];
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['talents'] = !empty($_COOKIE['telents_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
  
    // Выдаем сообщения об ошибках.
    if ($errors['fio'] == 'null') {
      setcookie('fio_error', '', 100000);
      $messages[] = '<div>Заполните имя.</div>';
    } else if ($errors['fio'] == 'incorrect') {
      setcookie('fio_error', '', 100000);
      $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
    }
  
    // email error print
    if ($errors['email']) {
      setcookie('email_error', '', 100000);
      $messages[] = '<div>Заполните почту.</div>';
    }
  
    // talentss error print
    if ($errors['talents']) {
      setcookie('talents_error', '', 100000);
      $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
    }
  
    if ($errors['biography']) {
      setcookie('biography_error', '', 100000);
      $messages[] = '<div>Напишите что-нибудь о себе.</div>';
    }
  
    if ($errors['check']) {
      setcookie('check_error', '', 100000);
      $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
    }
  
  
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['gender_value'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['biography_value'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    $values['abil_value'] = empty($_COOKIE['abil_value']) ? '' : $_COOKIE['abil_value'];
    $values['data_value'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
    $values['limbs_value'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];

    $talents = array();
    $values = array();
    $powers = array();
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (
      !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])
    ) {
      // TODO: загрузить данные пользователя из БД
      // и заполнить переменную $values,
      // предварительно санитизовав.
      $user = 'u35651';
      $pass = '2345345';
      $db = new PDO('mysql:host=localhost;dbname=u35651', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      $user = $db->prepare("SELECT * FROM user WHERE userId = ?");
      $user->execute([$_SESSION['uId']]);
      $row = $user->fetch(PDO::FETCH_ASSOC);
  
      $values['fio'] = $row["name"];
      $values['email'] = $row["email"];
      $values['data'] = $row["date"];
      $values['gender'] = $row["gender"];
      $values['limbs'] = $row["amountOFColumn"];
      $values['biography'] = $row["biography"];
  
      //достаем способности из бд
      $userPower = $db->prepare("SELECT powerId FROM userPower WHERE userId = ?");
      $userPower->execute([$_SESSION['uId']]);
      $powers = $userPower->fetchAll(PDO::FETCH_COLUMN);
      $userPower = $db->prepare("SELECT description FROM power WHERE powerId = ?");
      foreach ($powers as $power) {
        $userPower->execute([$power]);
        $talents[$power] = $userPower->fetch(PDO::FETCH_COLUMN);
      }
      $values['talents'] = [];
      $values['talents'] = $talents;
  
  
      $messages[10] = sprintf('Вход с логином <strong>%s</strong>, uId <strong>%d</strong>',
      strip_tags($_SESSION['login']),
      strip_tags($_SESSION['uId']));
    }
    else {

        $talents['1'] = "Скорочтение";
        $talents['2'] = "Хорошая физическая форма";
        $talents['3'] = "Фотографическая память";
    
        $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
        $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
        $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
        $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
        $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
        $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    
    
        if (!empty($_COOKIE['talents_value'])) {
          $talents_value = json_decode($_COOKIE['talents_value']);
        }
        $values['talents'] = [];
        if (isset($talents_value) && is_array($talents_value)) {
          foreach ($talents_value as $power) {
            if (!empty($talents[$power])) {
              $values['talents'][$power] = $power;
            }
          }
        }
      }
      // Включаем содержимое файла form.php.
      // В нем будут доступны переменные $messages, $errors и $values для вывода
      // сообщений, полей с ранее заполненными данными и признаками ошибок.
      include('form.php');

}
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
  
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('fio_error', 'null', time() + 24 * 60 * 60);
      $errors = TRUE;
    } else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["fio"])) {
      setcookie('fio_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
    } else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
  
    if (empty($_POST['email'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('email_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    } else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
  
  
  
    if (empty($_POST['biography'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    } else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }
  
    if (empty($_POST['check'])) {
      // Выдаем куку на день с флажком об ошибке в поле name.
      setcookie('check_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    $talents = array();
    foreach ($_POST['talents'] as $key => $value) {
      $talents[$key] = $value;
    }
    if (!sizeof($talents)) {
      setcookie('talents_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    } else {
      setcookie('talents_value', json_encode($talents), time() + 30 * 24 * 60 * 60);
    }
  
    setcookie('data_value', $_POST['data'], time() + 30 * 24 * 60 * 60);
    setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  
  
    if ($errors) {
      // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
      header('Location: index.php');
      exit();
    } else {
      // Удаляем Cookies с признаками ошибок.
      setcookie('fio_error', '', 100000);
      setcookie('email_error', '', 100000);
      setcookie('talents_error', '', 100000);
      setcookie('biography_error', '', 100000);
      setcookie('check_error', '', 100000);
      $user = 'u35651';
      $pass = '2345345';
      $name = $_POST['fio'];
      $email = $_POST['email'];
      $data = $_POST['data'];
      $gender = $_POST['gender'];
      $limbs = $_POST['limbs'];
      $biography = $_POST['biography'];
      $talents = array();
      $talents = $_POST['talents'];
    
      $flag = false;
      $db = new PDO('mysql:host=localhost;dbname=u35651', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
      if (
        !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])
      ) {
        // TODO: перезаписать данные в БД новыми данными,
        // кроме логина и пароля.
        $flag = true;
      }
    
      if ($flag == true) {
        try {
        $userId = $_SESSION['uId'];
        $user = $db->prepare("UPDATE user set name = ?, email = ?, date = ?, gender = ?, amountOFColumn = ?, biography = ? WHERE userId = ?");
        $user->execute([$name, $email, $data, $gender, $limbs, $biography, $userId]);
        $userPower = $db->prepare("UPDATE userPower set powerId = ? WHERE userId = ?");
        foreach ($talents as $ability) {
          $userPower->execute([$ability, $userId]);
        }
        }
         catch (PDOException $e) {
          printf($e);
          setcookie('notsave', '1');
        }
        if(!empty($_COOKIE['admin'])){
            header('Location: ./admin.php');
            exit();
          }
      } else {
        // Генерируем уникальный логин и пароль.
        // TODO: сделать механизм генерации, например функциями rand(), uniquId(), md5(), substr().
        // Сохраняем в Cookies.
        // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
        // ...
        try {
          $user = $db->prepare("INSERT INTO user set name = ?, email = ?, date = ?, gender = ?, amountOFColumn = ?, biography = ?");
          $user->execute([$name, $email, $data, $gender, $limbs, $biography]);
          $userId = $db->lastInsertId();
          $userPower = $db->prepare("INSERT INTO userPower (userId, powerId) VALUES (:userId, :powerId)");
          foreach ($talents as $ability) {
            $userPower->execute(array('userId' => $userId, 'powerId' => $ability));
          }
    
          $login = $userId . $name;
          $passId = rand(10, 100);
    
          $stmt = $db->prepare("INSERT INTO loginData SET userId = ?, loginId = ?, passId = ?");
          $stmt->execute([$userId, $login, md5($passId)]);
        } catch (PDOException $e) {
          printf($e);
          setcookie('notsave', '1');
        }
    
        setcookie('login', $login);
        setcookie('pass', $passId);
      }
      // Сохраняем куку с признаком успешного сохранения.
      setcookie('save', '1');
    
      header('Location: ?save=1');
    }
}
